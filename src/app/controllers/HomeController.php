<?php

namespace Dzion\App\Controllers;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Dzion\Engine\BaseController;

//use Dzion\Engine\Request;
//use Dzion\Engine\Response;


class HomeController extends BaseController
{
    public function index(Request $request, Response $response, array $args = [])
    {
        $result = '<html><body><h1>Index action!</h1>
                               <a href="about">О нас!</a>
                   </body></html>';
        return $response->setContent($result);
    }

    public function about(Request $request, Response $response, array $args = [])
    {
        // dump($request->getBaseUrl());
        $url = $request->getBaseUrl();
        $result = '<html><body><h1>About action!</h1>
                               <a href="' .$url. '"> На главную!</a>
                   </body></html>';
        return $response->setContent($result);
    }

}