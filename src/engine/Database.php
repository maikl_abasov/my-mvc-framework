<?php

namespace Dzion\Engine;

class Database
{
    protected  $pdo;
    protected  $config;

    public function __construct(array $config) {
        $this->config = $config;
    }

    public function connect() {
        $driver = (!empty($this->config['driver'])) ? $this->config['driver'] : 'mysql';
        $host   = $this->config['host'];
        $dbname = $this->config['dbname'];
        $user   = $this->config['user'];
        $password   = $this->config['password'];
        $dsn =  $driver . ':host=' .$host. ';dbname=' .$dbname. ';charset=utf8';
        $this->pdo = new \PDO($dsn, $user, $password);
    }
}