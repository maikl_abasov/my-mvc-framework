<?php

namespace Dzion\Engine;

use Dzion\Engine\Database;
use Dzion\Engine\Router;
use Dzion\Engine\Request;

class AppKernel
{

    protected $dbConf;

    public function __construct(array $dbConfig) {
        $this->dbConf = $dbConfig;
    }

    public function init() {

        $db = new Database($this->dbConf);

        $router = new Router();

        $router->addRoute('GET', '/', 'Dzion\App\Controllers\HomeController::index');
        $router->addRoute('GET', '/about', 'Dzion\App\Controllers\HomeController::about');
        // $router->addRoute('GET', '/{link}!{from}', 'App\HomeController::about');
        // $router->addRoute('POST', '/', 'Rayac\searchController::find');
        $dispatcher = $router->getDispatcher();

        // dump($dbConf);

        $request = Request::createFromGlobals();

        $response = $dispatcher->dispatch($request->getMethod(), $request->getPathInfo());
        $response->send();

    }

}