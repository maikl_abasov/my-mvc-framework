<?php

require '../vendor/autoload.php';

// use Symfony\Component\HttpFoundation\Request;
// use Symfony\Component\HttpFoundation\Response;

define('ROOT_DIR'   ,  __DIR__ );
define('UPLOAD_DIR' ,  ROOT_DIR . '/../uploads/');
define('CONFIG_DIR' ,  ROOT_DIR . '/../configs/');
define('LOG_DIR'    ,  ROOT_DIR . '/../logs/');
define('SRC_DIR'    ,  ROOT_DIR . '/../src/');

$dbConf = [
    'driver'   => 'mysql',
    'host'     => 'localhost',
    'dbname'   => 'db_name',
    'user'     => 'root',
    'password' => 'password',
];

$app = new Dzion\Engine\AppKernel($dbConf);

$app->init();

//$db = new Dzion\Engine\Database($dbConf);
//
//$router = new Dzion\Engine\Router();
//
//$router->addRoute('GET', '/', 'Dzion\App\Controllers\HomeController::index');
//$router->addRoute('GET', '/about', 'Dzion\App\Controllers\HomeController::about');
//// $router->addRoute('GET', '/{link}!{from}', 'App\HomeController::about');
//// $router->addRoute('POST', '/', 'Rayac\searchController::find');
//$dispatcher = $router->getDispatcher();
//
//// dump($dbConf);
//
//$request = Dzion\Engine\Request::createFromGlobals();
//
//$response = $dispatcher->dispatch($request->getMethod(), $request->getPathInfo());
//$response->send();
